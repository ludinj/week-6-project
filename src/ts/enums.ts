export enum Pages {
  HOME = '/',
  GAMES = 'games',
  LOGIN = 'login',
}
export enum ReducerAction {
  START = 'start',
  SUCCESS = 'success',
  ERROR = 'error',
}
export enum ArrowAction {
  LEFT = 'left',
  RIGHT = 'right',
}
