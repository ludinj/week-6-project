export interface IGenre {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
}

export interface IPlatform {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
  logo?: any;
}

export interface IPublisher {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
}

export interface IComment {
  id: number;
  trainee?: any;
  game: number;
  body: string;
  created_at: Date;
  updated_at: Date;
  user: IUserData;
  username?: string;
}

export interface IGame {
  id: number;
  name: string;
  genre: IGenre;
  release_year: number;
  price: string;
  created_at: Date;
  updated_at: Date;
  cover_art?: any;
  platforms: IPlatform[];
  publishers: IPublisher[];
  comments: Comment[];
}
export interface IRole {
  id: number;
  name: string;
  description: string;
  type: string;
}

export interface IUserData {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  role: IRole;
  created_at: Date;
  updated_at: Date;
  firstName: string;
  lastName: string;
  comments: Comment[];
}

export interface IUser {
  jwt: string;
  user: IUserData;
}
export interface IApiResponse<T> {
  loading: boolean;
  data: T | null;
  error: Error | null;
}
