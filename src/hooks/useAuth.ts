import { useState } from 'react';
import { IUser } from '../ts/interfaces';
import { BASE_API_URL } from '../services/constants';

const storedUser = JSON.parse(`${localStorage.getItem('user')}`);

export const useAuth = (action?: string) => {
  const [user, setUser] = useState<IUser | null>(storedUser);
  const [error, setError] = useState<any | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const login = async (username: string, password: string) => {
    setLoading(true);
    try {
      const response = await fetch(`${BASE_API_URL}/auth/local`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({ identifier: username, password: password }),
      });
      if (response.ok) {
        const user: IUser = await response.json();
        localStorage.setItem('user', JSON.stringify(user));
        setUser(user);
        setError(null);
        window.location.reload();
        return user;
      } else {
        const error = await response.json();
        setError(error);
      }
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };
  const logout = () => {
    setUser(null);
    localStorage.removeItem('user');
    window.location.reload();
  };
  return { user, login, loading, error, logout };
};
