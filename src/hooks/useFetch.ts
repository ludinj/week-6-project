import { useEffect, useReducer } from 'react';
import { IApiResponse } from '../ts/interfaces';
import { ReducerAction } from '../ts/enums';
type Action<T> =
  | { type: ReducerAction.START }
  | { type: ReducerAction.ERROR; payload: Error }
  | { type: ReducerAction.SUCCESS; payload: T };

export const useFetch = <T>(url: string, options?: RequestInit) => {
  const initialState: IApiResponse<T> = {
    loading: false,
    data: null,
    error: null,
  };
  const reducer = (state: IApiResponse<T>, action: Action<T>) => {
    switch (action.type) {
      case ReducerAction.START:
        return {
          loading: true,
          data: null,
          error: null,
        };
      case ReducerAction.SUCCESS:
        return {
          loading: false,
          data: action.payload,
          error: null,
        };
      case ReducerAction.ERROR:
        return {
          loading: false,
          data: null,
          error: action.payload,
        };

      default:
        return state;
    }
  };
  const [response, dispatch] = useReducer(reducer, initialState);
  useEffect(() => {
    const controller: AbortController = new AbortController();
    const signal: AbortSignal = controller.signal;
    const fetchData = async () => {
      dispatch({ type: ReducerAction.START });
      try {
        const response: Response = await fetch(url, {
          signal: signal,
          ...options,
        });
        const data = await response.json();
        dispatch({
          type: ReducerAction.SUCCESS,
          payload: data,
        });
      } catch (error: any) {
        if (error.name !== 'AbortError') {
          dispatch({
            type: ReducerAction.ERROR,
            payload: error as Error,
          });
        }
      }
    };
    fetchData();
    return () => {
      controller.abort();
    };
  }, [url, options]);
  return response;
};
