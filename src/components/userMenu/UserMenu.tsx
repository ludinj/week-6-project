import React from 'react';
import { useAuth } from '../../hooks/useAuth';
import './userMenu.scss';
const UserMenu = () => {
  const { logout } = useAuth();
  return (
    <div className='user-menu__container'>
      <ul>
        <li onClick={logout}>Logout</li>
      </ul>
    </div>
  );
};

export default UserMenu;
