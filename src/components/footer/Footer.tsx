import React from 'react';
import './footer.scss';
import brandImg from '../../assets/brand.png';
import { AiFillGithub, AiFillTwitterCircle } from 'react-icons/ai';
const Footer = () => {
  return (
    <footer className='footer'>
      <div className='footer__content'>
        <img src={brandImg} alt='' />
        <p> © 2022 All Rights Reserved.</p>
        <div className='icons'>
          <AiFillGithub />
          <AiFillTwitterCircle />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
