import Card from '../card/Card';
import './grid.scss';
import { IGame } from '../../ts/interfaces';

type GridProps = {
  games: IGame[];
};
const Grid = ({ games }: GridProps) => {
  return (
    <div className='grid__wrapper'>
      <div className='grid__content'>
        {!games && <h4>Loading...</h4>}
        {games && games.map((game) => <Card game={game} key={game.id} />)}
      </div>
    </div>
  );
};

export default Grid;
