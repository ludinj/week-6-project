import React from 'react';
import './comment.scss';
import { IComment } from '../../ts/interfaces';

type CommentProps = {
  comment: IComment;
};
const NonMemoComment = ({ comment }: CommentProps) => {
  const date = new Date(comment.created_at).toDateString();

  return (
    <div className='comment'>
      <h4>{comment.user.username}</h4>
      <p>{comment.body}</p>
      <span>{date}</span>
    </div>
  );
};

export const Comment = React.memo(NonMemoComment);
