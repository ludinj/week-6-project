import brandImage from '../../assets/brand.png';
import { Link } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import { useState } from 'react';
import { Pages } from '../../ts/enums';
import UserMenu from '../userMenu/UserMenu';
import './navbar.scss';

const Navbar = () => {
  const { user } = useAuth();
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  return (
    <nav className='navbar'>
      <div className='navbar__content'>
        <div className='navbar__brand'>
          <img src={brandImage} alt='brand' />
        </div>
        <ul>
          <Link to={'/'} className='nav__item'>
            Home
          </Link>
          <Link to={`/${Pages.GAMES}`} className='nav__item'>
            Games
          </Link>
        </ul>
        <div className='user'>
          <div className='login'>
            {!user ? (
              <Link to={`/${Pages.LOGIN}`}>
                <p>Login</p>
              </Link>
            ) : (
              <p onClick={() => setIsMenuOpen((pre) => !pre)}>
                {user.user.username}
              </p>
            )}
          </div>
          {isMenuOpen && <UserMenu />}
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
