import React from 'react';
import './card.scss';
import noImageFound from '../../assets/no-image-found.png';
import { IGame } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';
import { Pages } from '../../ts/enums';
type CardProps = {
  game: IGame;
};
const Card = ({ game }: CardProps) => {
  const cardImage = game.cover_art?.url;
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/${Pages.GAMES}/${game.id}`, { state: { game } });
  };

  return (
    <div className={`card`} onClick={handleClick}>
      <img src={game.cover_art ? cardImage : noImageFound} alt='cardImage' />
      <div className='card__info'>
        <div className='info__title'>
          <h4>{game.name}</h4>
        </div>
        <div className='info__footer'>
          <span>Genre: {game.genre.name}</span>
          <span>Price: ${game.price}</span>
        </div>
      </div>
    </div>
  );
};

export default Card;
