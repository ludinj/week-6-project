import { Navigate, Outlet } from 'react-router-dom';
import { IUser } from '../../ts/interfaces';

type ProtecTedRouteProps = {
  user: IUser | null;
};
export const ProtectedRoute = ({ user }: ProtecTedRouteProps) => {
  return !user ? <Outlet /> : <Navigate to='/' replace />;
};
