import React, { useMemo } from 'react';
import { postsPerPage } from '../../services/constants';
import { ArrowAction } from '../../ts/enums';
import {
  BsFillArrowLeftCircleFill,
  BsFillArrowRightCircleFill,
} from 'react-icons/bs';
import './pagination.scss';

type PaginationProps = {
  totalGames: number;
  setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
  currentPage: number;
};

const Pagination = ({
  totalGames,
  setCurrentPage,
  currentPage,
}: PaginationProps) => {
  const pages = useMemo(() => {
    const pages = [];
    for (let i = 1; i <= Math.ceil(totalGames / postsPerPage); i++) {
      pages.push(i);
    }
    return pages;
  }, [totalGames]);

  const handleArrowClick = (action: string) => {
    window.scroll(0, 0);
    if (action === ArrowAction.LEFT && currentPage > 1) {
      setCurrentPage((prev) => prev - 1);
    }
    if (action === ArrowAction.RIGHT && pages.length > currentPage) {
      setCurrentPage((prev) => prev + 1);
    }
  };
  const handleButtonClick = (page: number) => {
    window.scroll(0, 0);
    setCurrentPage(page);
  };
  return (
    <div className='pagination'>
      {currentPage !== 1 && (
        <BsFillArrowLeftCircleFill
          className='arrow'
          onClick={() => handleArrowClick(ArrowAction.LEFT)}
        />
      )}
      <div className='pages'>
        {pages.map((page, index) => {
          return (
            <button
              key={index}
              onClick={() => handleButtonClick(page)}
              className={page === currentPage ? 'active' : ''}
            >
              {page}
            </button>
          );
        })}
      </div>
      {currentPage !== pages.length && (
        <BsFillArrowRightCircleFill
          className='arrow'
          onClick={() => handleArrowClick(ArrowAction.RIGHT)}
        />
      )}
    </div>
  );
};

export default Pagination;
