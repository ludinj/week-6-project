import LoginPage from './pages/loginPage/LoginPage';
import Home from './pages/home/Home';
import DetailsPage from './pages/detailsPage/DetailsPage';
import GamesList from './pages/gamesList/GamesList';
import Footer from './components/footer/Footer';
import Navbar from './components/navbar/Navbar';
import { Route, Routes } from 'react-router-dom';
import { Pages } from './ts/enums';
import { useAuth } from './hooks/useAuth';
import { ProtectedRoute } from './components/protectedRoute/ProtectedROute';
const App: React.FC = () => {
  const { user } = useAuth();
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path={`/${Pages.HOME}`} element={<Home />} />
        <Route path={`/${Pages.GAMES}`} element={<GamesList />} />
        <Route element={<ProtectedRoute user={user} />}>
          <Route path={`/${Pages.LOGIN}`} element={<LoginPage />} />
        </Route>
        <Route path={`/${Pages.GAMES}/:gameId`} element={<DetailsPage />} />
      </Routes>
      <Footer />
    </div>
  );
};

export default App;
