import { IComment } from '../ts/interfaces';
import { postsPerPage } from './constants';

//if the current page is 1 set the start to 0 to render the first 6 post.
export const calculateStart = (currentPage: number) => {
  if (currentPage === 1) {
    return 0;
  } else {
    return (currentPage - 1) * postsPerPage;
  }
};

export const sortComments = (comments: IComment[]) => {
  const sortedComments = comments.sort((a, b) => {
    return +new Date(b.created_at) - +new Date(a.created_at);
  });
  return sortedComments;
};
