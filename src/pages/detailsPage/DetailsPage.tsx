import { IComment, IPlatform } from '../../ts/interfaces';
import { useLocation } from 'react-router-dom';
import noImage from '../../assets/no-image-found.png';
import './detailsPage.scss';
import { useAuth } from '../../hooks/useAuth';
import React, { useEffect, useRef, useState } from 'react';
import { BASE_API_URL } from '../../services/constants';
import { Comment } from '../../components/comment/Comment';
import { useFetch } from '../../hooks/useFetch';
import { sortComments } from '../../services/utils';

const DetailsPage = () => {
  const [comments, setComments] = useState<IComment[]>([]);
  const [isButtonVisible, setIsButtonVisible] = useState<boolean>(false);
  const [newCommentText, setNewCommentText] = useState<string>('');
  const { state } = useLocation();
  const { game } = state;
  const { user } = useAuth();
  const { data, loading } = useFetch<IComment[]>(
    `${BASE_API_URL}/games/${game.id}/comments/?_limit=-1`
  );
  const inputRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    if (data) {
      const sortedComments = sortComments(data);
      setComments(sortedComments.slice(0, 10));
    }
  }, [data]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (inputRef.current) {
      inputRef.current.value = e.target.value;
      setNewCommentText(inputRef.current.value);
      if (inputRef.current.value !== '') {
        setIsButtonVisible(true);
      } else {
        setIsButtonVisible(false);
      }
    }
  };

  const postComment = async () => {
    if (user) {
      try {
        const response = await fetch(
          `${BASE_API_URL}/games/${game.id}/comment`,
          {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${user.jwt}`,
              'Content-type': 'application/json',
            },
            body: JSON.stringify({ body: newCommentText }),
          }
        );
        if (inputRef.current) inputRef.current.value = '';
        const newComment = await response.json();
        setComments((pre) => [newComment, ...pre]);
        setIsButtonVisible(false);
      } catch (error) {
        alert(error);
      }
    } else {
      alert('You must Login to comment');
    }
  };

  return (
    <div className='detailsPage__wrapper'>
      <div className='card__container'>
        <div className='image'>
          <img
            src={game.cover_art ? game.cover_art.url : noImage}
            alt='cardImage'
          />
        </div>
        <div className='details'>
          <h4>{game.name}.</h4>
          <p>Release Year: {game.release_year}</p>
          <p>Genre: {game.genre.name}</p>
          <p>
            Release for:
            {game.platforms.map((platform: IPlatform) => (
              <span key={platform.id}>{platform.name}</span>
            ))}
          </p>
          <p>Price: ${game.price}</p>
        </div>
      </div>
      <section className='comments__container'>
        <div className='comment__input'>
          <input
            type='text'
            placeholder='Agrega un comentario…'
            onChange={handleChange}
            ref={inputRef}
          />
          {isButtonVisible && (
            <button type='button' onClick={postComment}>
              Comentar
            </button>
          )}
        </div>
        <div className='comments'>
          {!loading ? (
            comments.map((comment: IComment, idx: number) => (
              <Comment comment={comment} key={idx} />
            ))
          ) : (
            <h4>Loading..</h4>
          )}
        </div>
      </section>
    </div>
  );
};

export default DetailsPage;
