import React, { useState } from 'react';

import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import { Pages } from '../../ts/enums';
import './loginPage.scss';

const LoginPage = () => {
  const [inputData, setInputData] = useState({ identifier: '', password: '' });
  const [errorMessage, setErrorMessage] = useState<string>('');
  const { login, loading, error } = useAuth();
  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputData((pre) => ({ ...pre, [e.target.name]: e.target.value.trim() }));
  };
  useEffect(() => {
    const errorMessage = error?.message[0]?.messages[0]?.message;
    setErrorMessage(errorMessage);
  }, [error]);

  const handleLogin = async (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    try {
      const user = await login(inputData.identifier, inputData.password);
      if (user) {
        navigate(`${Pages.HOME}`);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className='login__wrapper'>
      <form onSubmit={handleLogin}>
        <h1>Login</h1>
        <div className='input__box'>
          <label htmlFor='identifier'>Username</label>
          <input type='text' name='identifier' onChange={handleChange} />
        </div>
        <div className='input__box'>
          <label htmlFor='password'>Password</label>
          <input type='password' name='password' onChange={handleChange} />
        </div>
        {error && <span>{errorMessage}</span>}
        <button type='submit' disabled={loading}>
          LOGIN
        </button>
      </form>
    </div>
  );
};

export default LoginPage;
