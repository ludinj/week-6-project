import { useEffect, useState } from 'react';
import './gamesList.scss';
import { IGame } from '../../ts/interfaces';
import { BASE_API_URL } from '../../services/constants';
import { postsPerPage } from '../../services/constants';
import { useFetch } from '../../hooks/useFetch';
import Grid from '../../components/grid/Grid';
import { calculateStart } from '../../services/utils';
import Pagination from '../../components/pagination/Pagination';

const GamesList = () => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [start, setStart] = useState<number>(0);
  const [totalGames, setTotalGames] = useState<number>(0);
  const { data } = useFetch<IGame[]>(
    `${BASE_API_URL}/games/?_start=${start}&_limit=${postsPerPage}`
  );

  useEffect(() => {
    const start = calculateStart(currentPage);
    setStart(start);
  }, [currentPage]);

  useEffect(() => {
    const fetchNumberOfGames = async () => {
      try {
        const totalGames = await fetch(`${BASE_API_URL}/games/count`);
        setTotalGames(await totalGames.json());
      } catch (error) {
        alert(error);
      }
    };
    fetchNumberOfGames();
  }, []);

  return (
    <div className='wrapper'>
      {!data ? <h4>Loading...</h4> : <Grid games={data} />}
      <Pagination
        totalGames={totalGames}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </div>
  );
};

export default GamesList;
