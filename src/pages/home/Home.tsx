import minecraftBg from '../../assets/minecraft.jpg';
import brand from '../../assets/brand.png';
import { BsFillArrowRightCircleFill } from 'react-icons/bs';
import { Pages } from '../../ts/enums';
import { Link } from 'react-router-dom';
import './home.scss';
const Home = () => {
  return (
    <div className='home__wrapper'>
      <img src={minecraftBg} alt='' />
      <div className='overlay'>
        <div className='info__card'>
          <img src={brand} alt='' />
          <p>
            Find and play the best games for you,{' '}
            <strong>Is time to have fun!</strong>
          </p>
          <span>We believe in community</span>
          <Link to={`/${Pages.GAMES}`}>
            <button>
              <BsFillArrowRightCircleFill />
              Explore games
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
